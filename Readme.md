# Send Message
Aplicación que envia datos desde una activity a otra. 

En este caso se usa la clase [Intent](https://developer.android.com/reference/android/content/Intent) en el cual incluimos un Bundle con los datos. En dicho [Bundle](https://developer.android.com/reference/android/os/Bundle) van dos extras (llamando al método putString()) en par de "key" y valor de la misma.



También contiene lo necesario para crear un JavaDoc pare ello habría que ir a Tools/Generate Javadoc... (Se puede ver el ejemplo ya creado en la carpeta docs).

Como optimización de la misma se empieza añadiendo todo en un Bundle para luego más tarde acabar con una clase modelo o POJO (message) la cual implementa [Parcelable](https://developer.android.com/reference/android/os/Parcelable), encapsulando así todos los campos que vamos a enviar.

Se añade la librería [Logger](https://github.com/orhanobut/logger) en la clase SendMessageActivity para mostrar el Log de forma más ordenada.

También se añade lo siguiente en el manifiest:

    <!-- 1.- Solicitud de permisos de los recursos que necesita la aplicación -->
    <uses-permission android:name="android.permission.INTERNET"/>
    
    <!-- 2.- Información de los recursos que usa nuestra aplicación a Google Play -->
    <uses-feature android:name="android.hardware.camera2"/>
