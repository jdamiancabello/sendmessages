package es.jdamiancabello.sendmessage;

import android.app.Application;
import android.util.Log;

import es.jdamiancabello.sendmessage.jdamiancabello.User;

public class SendMessageApplication extends Application {
    private static final String TAG = "SendMessageApplication";
    private User user;

    public User getUser() {
        return user;
    }

    /**
     * Este método se crea el primero antes de inicializar cualquier componente.
     */
    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG,"Application onCreate");
        user = new User("Damián","Damian password");
    }

    /**
     * Este método no se ejecuta nunca.
     */
    @Override
    public void onTerminate() {
        super.onTerminate();
        Log.d(TAG,"Application onTerminate");
    }
}
