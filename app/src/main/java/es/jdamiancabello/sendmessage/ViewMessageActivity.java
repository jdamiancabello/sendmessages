package es.jdamiancabello.sendmessage;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import es.jdamiancabello.sendmessage.jdamiancabello.Message;

/**
 * <h1>ViewMessageActivity</h1>
 * <p>Clase que muestra el contenido recibe un objeto de otra clase de apoyo Message</p>
 * </br>
 * <p>También muestra logs en los métodos onStart(), onStop(), y onResume().</p>
 *
 * @author damian
 * @version 1.0
 * @see Message
 * @see Log
 */

public class ViewMessageActivity extends AppCompatActivity {

    //region Variables de la clase

    TextView tvAuthor;
    TextView tvMessage;
    private static final String TAG = "ViewMessageActivity";

    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_message);

        //region Forma antigua de crear el Bundle pasando cada uno de los datos

        //1.- Recoger el objeto Bundle
        //Bundle bundle = getIntent().getExtras();

        //2.- Asignar las cadenas a los componentes View.
        //tvAuthor.setText(bundle.getString("author"));
        //tvMessage.setText(bundle.getString("message"));

        //endregion

        //region Forma optima de enviar datos por el Bundle mediante clase modelo

        //1.- Recoger el objeto Bundle
        Message message = getIntent().getExtras().getParcelable("message");

        //2.- Asignar las cadenas a los componentes View.
        tvAuthor = findViewById(R.id.tvAutor);
        tvMessage = findViewById(R.id.tvMessage);


        tvAuthor.setText(message.get_author());
        tvMessage.setText(message.getMessage());

        //endregion

    }
    //region Métodos sobreescritos para mostrar Logs con la clase Log

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "ViewMessageActivity -> onStart");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "ViewMessageActivity -> onStop");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "ViewMessageActivity -> onResume");
    }

    //endregion
}
