package es.jdamiancabello.sendmessage.jdamiancabello;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * <h1>Message</h1>
 * <p>Clase POJO que se usa para intercambiar datos entre Activitys.</p>
 * </br>
 * <p>Es una clase modelo que gestiona la estructura de un mensaje.</p>
 * <p>Implementa parcelable, aumentando así su facilidad de lectura/escritura de la misma.</p>
 * </br>
 * <a href="http://www.developerphil.com/parcelable-vs-serializable/">Diferencias serializable y parcelable (Inglés)</a>
 *
 * @author damian
 * @version 1.0
 * @see Parcelable
 */

public class Message implements Parcelable {

    private String _author;
    private String _message;

    //region Constructor

    /*
    Si se quiere utilizar el contructor vacio y hubiera otro constructor se tiene que implementar. Si no hay constructor
    Java lo crea por defecto automático.
     */

    public Message(String _author, String message) {
        this._author = _author;
        this._message = message;
    }

    //endregion

    //region Getters de la clase (No se necesitan Setters)

    public String get_author() {
        return _author;
    }
    public String getMessage() {
        return _message;
    }

    //endregion

    //region Métodos sobreescritos de la clase Parcelable

    protected Message(Parcel in) {
        _author = in.readString();
        _message = in.readString();
    }

    public static final Creator<Message> CREATOR = new Creator<Message>() {
        @Override
        public Message createFromParcel(Parcel in) {
            return new Message(in);
        }

        @Override
        public Message[] newArray(int size) {
            return new Message[size];
        }
    };

    @Override
    public String toString() {
        return _author + " : " + _message;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(_author);
        parcel.writeString(_message);
    }

    //endregion
}
