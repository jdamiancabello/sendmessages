package es.jdamiancabello.sendmessage;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import es.jdamiancabello.sendmessage.jdamiancabello.Message;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.Logger;

/**
 * <h1>SendMessageActivity</h1>
 * <p>Clase que envia el contenido de un mensaje de una Activity a otra.
 * Para ello hace uso de la clase POJO Message en la cual encapsula la información que va a enviar.</p>
 * </br>
 * <p>También muestra logs en los métodos onStart(), onStop(), y onResume().</p>
 * <p>Para los logs se usa la librería Logger.</p>
 *
 * @author damian
 * @version 1.0
 * @see Message
 * @see Logger
 * @see AppCompatActivity
 * @see Bundle
 *
 */


public class SendMessageActivity extends AppCompatActivity {

    //region Variables de la clase

    EditText edMessage;
    Button btSend;
    private static final String TAG = "SendMessageActivity";

    //endregion

    //region Métodos sobreescritos para mostrar Logs con la clase Logger

    @Override
    protected void onStart() {
        super.onStart();
        Logger.d(TAG,"SendMessageActivity -> onStart");

        //Muestra un mensaje indicando el usuario que ha iniciado la aplicación
        showToast("Datos de usuario: " + ((SendMessageApplication)getApplication()).getUser().toString());
    }

    @Override
    protected void onStop() {
        super.onStop();
        Logger.d(TAG,"SendMessageActivity -> onStop");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Logger.d(TAG,"SendMessageActivity -> onResume");
    }

    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_message);

        //Buscamos los objetos instanciados arriba en las vistas
        edMessage = findViewById(R.id.edMessage);
        btSend = findViewById(R.id.btSend);


        //Iniciar el adapter para crear mensajes Log con formato.
        Logger.addLogAdapter(new AndroidLogAdapter());

        //Registrar un Listener para eventos (Siempre es con el SetOn...)
        btSend.setOnClickListener(new View.OnClickListener() { //"new View.OnClickListener()" Esto es una clase anónima, pues no tiene nombre, no puede ser llamada
            @Override
            public void onClick(View view) {
                if(!emptyMessage())
                    sendMessage();
                else
                    showError("Debes introducir un mensaje");
            }
        });
    }

    /**
    Método que comprueba que el usuario ha introducido un mensaje.
     */
    private boolean emptyMessage() {
        return edMessage.getText().toString().isEmpty();
    }

    /**
        Método que se ejecuta cuando ocurre el evento onClick de cualquier botón. Luego según su id ejecutará un método u otro.
     */
    public void sendMessage(View view) {
        switch (view.getId()) {//Esto es codigo espaguetti, NO USAR.
            case R.id.btSend:
                sendMessage();
                break;
            case R.id.btShow:
                showToast("Has pulsado el botón mostrar.");
                break;
        }
    }

    /**
     * Método que muestra un Toast al ser llamado.
     * @param message Texto que se muestra al usuario.
     */
    private void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    /**
     Método que muestra un error al usuario.
     @param message Texto que se muestra al usuario.
     */
    private void showError(String message) {
        Toast.makeText(this,message,Toast.LENGTH_LONG).show();
    }

    /**
     * Método lanzado por el evento onClick() del botón enviar
     */
    public void sendMessage(){


        //region Ejemplo de DownCasting
        /*
        if(view instanceof Button){
            Button button = ((Button)view);
            button.setText("Hola");
        }
        */
        //endregion

        //region Forma antigua, todos los objetos iban en en bundle separados (sin clase modelo)
        /*


        1.-Creamos un Bundle que contiene los datos que se comparten entre actividades.
        Bundle bundle = new Bundle();
        bundle.putString("author","Damián envia: ");
        bundle.putString("message",edMessage.getText().toString());


        2.- Creamos el objeto que envia el mensaje: Un Intent explícito donde se conoce la actividad origen y la actividad destino.
        Intent intent = new Intent(SendMessageActivity.this, ViewMessageActivity.class);

        3.- Añadir el objeto Bundle al Intent. Cuidado tal como se añade.
        intent.putExtras(bundle);

        4.- Iniciar la activity destino.
        startActivity(intent);



        */
        //endregion

        //region Forma con clase modelo

        //1.-Creamos un Bundle que contiene los datos que se comparten entre actividades.
        Bundle bundle = new Bundle();
        Message message = new Message("Damián envia: ",edMessage.getText().toString());
        bundle.putParcelable("message", message);

        //2.- Creamos el objeto que envia el mensaje: Un Intent explícito donde se conoce la actividad origen y la actividad destino.
        Intent intent = new Intent(SendMessageActivity.this, ViewMessageActivity.class);

        //3.- Añadir el objeto Bundle al Intent. Cuidado tal como se añade.
        intent.putExtras(bundle);

        //4.- Iniciar la activity destino.
        startActivity(intent);

        //endregion
    }

}
